// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const carsData = require("../CarsData.js");
const allCarBefore2000 = require("../Problem5.js");

try {
  const carYearBefore2000 = allCarBefore2000.countCarsOlderThan2000(
    carsData.inventory
  );

  if (!carYearBefore2000) {
    throw new Error("Data  is null, undefined, or empty");
  }

  console.log(`all car years ${carYearBefore2000} and it's length is ${carYearBefore2000.length}`);
} catch (error) {
  console.log(error.message);
}
