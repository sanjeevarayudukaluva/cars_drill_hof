const carsData = require("../CarsData.js");
const findCar = require("../Problem1.js");

try {
  const car_33 = findCar.findCarById(carsData.inventory, 33);

  if (!car_33) {
    throw new Error("Data for car 33 is null, undefined, or empty");
  }

  console.log(
    `Car 33 is a ${car_33.car_year} ${car_33.car_make} ${car_33.car_model}`
  );
} catch (error) {
  console.log(error.message);
}
