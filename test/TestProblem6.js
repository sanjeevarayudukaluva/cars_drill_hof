// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.
const carsData = require("../CarsData.js");
const audiBmwCars = require("../Problem6.js");

try {
  const onlyAudiAndBmwCars = audiBmwCars.getBMWAndAudiCars(carsData.inventory);

  if (!onlyAudiAndBmwCars) {
    throw new Error("Data  is null, undefined, or empty");
  }

  console.log(`all audi and bmw cars ${JSON.stringify(onlyAudiAndBmwCars)}`);
} catch (error) {
  console.log(error.message);
}
