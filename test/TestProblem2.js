// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:

const carsData = require("../CarsData.js");
const lastCarData = require("../Problem2.js");

try {
  const lastCar = lastCarData.getLastCarMakeAndModel(carsData.inventory);

  if (!lastCar) {
    throw new Error("Data  is null, undefined, or empty");
  }

  console.log(
    `Last car is a ${lastCar.car_make} ${lastCar.car_model}`
  );
} catch (error) {
  console.log(error.message);
}
