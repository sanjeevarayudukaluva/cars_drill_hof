// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

const carsData = require("../CarsData.js");
const allCarYears = require("../Problem4.js");

try {
  const carYears = allCarYears.getAllCarYears(carsData.inventory);

  if (!carYears) {
    throw new Error("Data  is null, undefined, or empty");
  }

  console.log(`all car years ${carYears}`);
} catch (error) {
  console.log(error.message);
}
