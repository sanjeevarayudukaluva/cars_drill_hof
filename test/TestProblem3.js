// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

const carsData = require("../CarsData.js");
const sortedCarModelsAlpha = require("../Problem3.js");

try {
  const sortedCarModels = sortedCarModelsAlpha.sortCarModelsAlphabetically(carsData.inventory);

  if (!sortedCarModels) {
    throw new Error("Data  is null, undefined, or empty");
  }

  console.log(
    `sorted car models ${sortedCarModels}`
  );
} catch (error) {
  console.log(error.message);
}
