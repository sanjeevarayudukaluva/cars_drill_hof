const sortCarModelsAlphabetically = (data) => {
  if (Array.isArray(data) && data.length !== 0) {
    return data.map((car) => car.car_model).sort();
  } else {
    return null;
  }
};
module.exports = { sortCarModelsAlphabetically };
