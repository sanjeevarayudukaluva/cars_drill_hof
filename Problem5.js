
const countCarsOlderThan2000 = (data) => {

    if (Array.isArray(data) && data.length !== 0) {
        let olderCars = data.filter(car => car.car_year < 2000);
    return olderCars.map(car => car.car_year);

      } else {
        return null;
      }
    
  };
  module.exports={countCarsOlderThan2000};