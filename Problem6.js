const getBMWAndAudiCars = (data) => {
  if (Array.isArray(data) && data.length !== 0) {
    return data.filter(
      (car) => car.car_make === "BMW" || car.car_make === "Audi"
    );
  } else {
    return null;
  }
};
module.exports = { getBMWAndAudiCars };
