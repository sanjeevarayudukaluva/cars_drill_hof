const findCarById = (data, id) => {
  if (Array.isArray(data) && data.length !== 0 && typeof id === "number") {
    return data.find((car) => car.id === id);
  } else {
    return null;
  }
};
module.exports = { findCarById };
