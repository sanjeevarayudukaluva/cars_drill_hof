const getAllCarYears = (data) => {
  if (Array.isArray(data) && data.length !== 0) {
    return data.map((car) => car.car_year);
  } else {
    return null;
  }
};
module.exports = { getAllCarYears };
