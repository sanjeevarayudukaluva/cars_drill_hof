const getLastCarMakeAndModel = (data) => {
  if (Array.isArray(data) && data.length !== 0 ) {
    const lastCar = data[data.length - 1];
    return lastCar;
  } else {
    return null;
  }
};
module.exports={getLastCarMakeAndModel};